#include "mbed.h"

Serial bt(D8, D2);  // declaration des pins utilisées parle module bluetooth, qui communique avec le microcontroleur en UART

PwmOut speed_A(D5);  // pins qui controllent la vitesse des moteurs
PwmOut speed_B(D6);

DigitalOut mot_A_1(D3);  // pins du sens 1 du pont en H du moteur A
DigitalOut mot_A_2(D4);  // pins du sens 2 du pont en H du moteur A

DigitalOut mot_B_1(D7);  // pins du sens 1 du pont en H du moteur B
DigitalOut mot_B_2(D8);  // pins du sens 2 du pont en H du moteur B


int main() {
    bt.baud(9600);

    // full steam ahead, moussaillon! Oï!
    speed_A = 0.2;
    speed_B = 0.2;
    
    char c;
    for(;;)
    {
        // l'application téléphone envoie un caractère différent selon la touche sur laquelle on appuie
        // on recupère donc ce caractère, et on éxécute l'action associée au bouton qui a envoyé le caractère
        c = bt.getc();  // getc() est bloquante, le programme attend ici tant qu'aucune commande n'est envoyée

        switch(c)  // on regarde la valeur du caractère
        {
        case 'u':  // un appui sur le bouton avancer a été fait
            // on fait passer le courant dans le même sens dans les deux moteurs

            // Note importante! Il ne faut pas fermer les deux paires du pont en H en même temps.
            mot_A_2 = 0;  // On coupe donc en premier celle qui n'est pas utilisée
            mot_A_1 = 1;  // On peut ensuite activer la paire qui va faire tourner le moteur dans le sens désiré (paire 1)

            mot_B_2 = 0;
            mot_B_1 = 1;
            break;
        case 'd':  // un appui sur le bouton reculer a été fait
            // on fait passer le courant dans l'autre sens dans les deux moteurs en activent l'autre moitié du pont en H
            mot_A_1 = 0;
            mot_A_2 = 1;  // on active l'autre paire du pont en H, le moteur tourne alors dans l'autre sens (paire 2)

            mot_B_1 = 0;
            mot_B_2 = 1;
            break;
        case 'r':  // un appui sur le bouton droit a été fait
            mot_A_2 = 0;
            mot_A_1 = 1;

            mot_B_1 = 0;
            mot_B_2 = 1;
            break;
        case 'l':  // un appui sur le bouton gauche a été fait
            mot_A_1 = 0;
            mot_A_2 = 1;

            mot_B_2 = 0;
            mot_B_1 = 1;
            break;
        case 's':
            mot_A_1 = 0;  // pour arreter le robot, on auvre toutes les paires. Il n'y a alors plus de courant qui passe dans le moteur
            mot_A_2 = 0;

            mot_B_2 = 0;
            mot_B_1 = 0;
            break;
        default:
            mot_A_1 = 0;
            mot_A_2 = 0;

            mot_B_2 = 0;
            mot_B_1 = 0;
            break;
        }
    }   
    
    return 0;
}